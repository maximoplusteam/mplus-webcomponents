import resolve from "rollup-plugin-node-resolve";
import commonjs from "rollup-plugin-commonjs";
import babel from "rollup-plugin-babel";

export default {
  input: "src/mplus-webcomponents.js",
  external: ["preact", "@skatejs/renderer-preact", "skatejs"],
  output: {
    file: "distrollup/bundle.js",
    format: "es",
    name: "webcomp",
    sourcemap: true
  },
  plugins: [
    resolve({
      jsnext: true,
      main: true
    }),
    commonjs({
      include: "node_modules/**"
    }),
    babel({
      exclude: ["node_modules/**"]
    })
  ]
};

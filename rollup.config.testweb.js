import resolve from "rollup-plugin-node-resolve";
import commonjs from "rollup-plugin-commonjs";
import babel from "rollup-plugin-babel";

export default {
  input: "src/mplus-webcomponents.js",
  output: {
    file: "distrollup/bundleweb.js",
    format: "iife",
    name: "webcomp",
    sourcemap: true
  },
  plugins: [
    resolve({
      jsnext: true,
      main: true
    }),
    commonjs({
      include: "node_modules/**"
    }),
    babel({
      exclude: ["node_modules/**"]
    })
  ]
};

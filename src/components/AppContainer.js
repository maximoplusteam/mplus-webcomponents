import { h } from "preact";
import { Component } from "./Component.js";
import { resolveContainer } from "../utils.js";

export class AppContainer extends Component {
  connected() {
    if (this.mp) return; //not garantueed to be called only once
    this.mp = new maximoplus.basecontrols.AppContainer(
      this.props.mboname,
      this.props.appname
    );
    //	kont[this.id]=this.mp;
    resolveContainer(this.props.id, this.mp);
    if (this.props.offlineenabled) {
      this.mp.setOfflineEnabled(true);
    }
  }

  disconnected() {
    this.mp.dispose();
    delete kont[this.id];
  }

  save() {
    this.mp.save();
  }

  rollback() {
    this.mp.rollback();
  }

  mboCommand(command) {
    this.mp.mboCommand(command);
  }

  mboSetCommand(command) {
    this.mp.mboSetCommand(command);
  }

  render() {
    return <div />;
  }

  static get props() {
    return {
      appname: { attribute: true },
      mboname: { attribute: true },
      wfprocess: { attribute: true },
      offlineenabled: { attribute: true },
      id: { attribute: true }
    };
  }
}

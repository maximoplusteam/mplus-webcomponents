import { h } from "preact";
import { Component } from "./Component.js";
import { props } from "skatejs";

export class AppRoot extends Component {
  //this will be the root component, where the global state and navigation will happen
  //also make this conigurable, so that users don't have to use this

  static get props() {
    return {
      dialogs: props.array
    };
  }

  render({ props }) {
    return (
      <div>
        <slot />
        <dialog-holder dialogs={props.dialogs} />
      </div>
    );
  }
}

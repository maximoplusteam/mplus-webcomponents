import { h } from "preact";
import { Component } from "./Component.js";
import { style } from "../utils.js";

export class BaseDialog extends Component {
  dialogStyle() {
    return `
.bottomdialog:{
margin:20px;
}
 .fadeMe {
    opacity:0.5;
    filter: alpha(opacity=50);
    background-color:#000; 
    width:100%; 
    height:100%; 
    z-index:10;
    top:0; 
    left:0; 
    position:fixed; 
  }
.popup {
    position: fixed; 
    left: 50%; top: 50%;
    width: 400px; 
    margin-left: -200px; margin-top: -150px;
    background: white; 
    padding: 10px;
    z-index: 11;
  }
`;
  }
  closeDialog() {
    let rootEl = getRootEl();
    let newRootElDialogs = rootEl.dialogs.slice();
    newRootElDialogs.pop();
    rootEl.dialogs = newRootElDialogs;
  }

  render() {
    return (
      <div>
        {style(this, this.dialogStyle())}
        <div>
          <div class="fadeMe" />
          <div class="popup">{this.renderDialogCallback()}</div>
        </div>
      </div>
    );
  }

  renderDialogCallback() {
    return <div />;
  }
}

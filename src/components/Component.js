import { props, withComponent } from "skatejs";
import withPreact from "@skatejs/renderer-preact";
import { h } from "preact";

import * as skate from "skatejs";

const BComponent = withComponent(withPreact());

export class Component extends BComponent {
  state = {};
  setState(okv) {
    let newState = Object.assign({}, this.state);
    for (const k in okv) {
      newState[k] = okv[k];
    }
    this.state = newState;
  }
}

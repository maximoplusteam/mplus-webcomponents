import { h } from "preact";
import { Component } from "./Component.js";
import { props } from "skatejs";
import { dialogs } from "./dialogs.js";

export class DialogHolder extends Component {
  static get props() {
    return {
      dialogs: props.array
    };
  }

  /*
     This is the temporary solution until I start working on the concrete implemetations, currently I wil keep displayed only one dialog in the holder per time. Maybe that should be enough for the transitions in case we open the dialog from the dialog(like filtering the data). If not, the other option would be to just keep the pages inside the dialog holder and display only one at the time
     */

  render({ props }) {
    if (!props.dialogs.legnth == 0) {
      return <div />;
    }
    let currDialog = props.dialogs[props.dialogs.length - 1];
    if (!currDialog) {
      return <div />;
    } else {
      return this.getJSXDialog(currDialog);
    }
  }

  getJSXDialog(dialog) {
    //input variable dialog is jsut the javascript object containtning the type of the dialog, container , templates , etc.
    //the output should be the JSX of the component,
    //and this  may be overriden for the concrete implementations. Probably the variable dialogs that has all the predefined dialogs doesn't need to change in the imoplementatins (because for example list-dialog control will point to the list-dialog of the implementation), but we need an optioon for the user to define the custom dialogs (for example, runing the command on non-persistent mboser), so I have to think more
    let _dialogs = Object.assign(this.getUserDefinedDialogs(), dialogs);
    return _dialogs[dialog.type](dialog);
  }

  getUserDefinedDialogs() {
    //this shouuld be override in concrete implementation
    return {}; //by default just the empty object. Concrete implemntation should return the obejct wiht the dialog names as the keys, and the JSX functions as values
  }
}

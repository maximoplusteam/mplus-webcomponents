import { h } from "preact";
import { MPlusComponent } from "./MplusComponent.js";
import { props } from "skatejs";
import { getDeferredContainer } from "../utils.js";

export class DocDownload extends MPlusComponent {
  static get props() {
    return {
      container: props.string,
      maxcontainer: props.object,
      baseurl: props.string
    };
  }

  putContainer(container) {
    this.mp = container;
    this.setInternalState(_ => {
      return {
        doclinks: new maximoplus.basecontrols.RelContainer(
          container,
          "doclinks"
        )
      };
    });
  }

  connected() {
    if (this.mp) {
      return;
    }

    if (this.container) {
      getDeferredContainer(this.container).then(container => {
        this.putContainer(container);
      });
    }
  }

  openDocument() {
    if (isCordovaApp && cordova.InAppBrowser) {
      cordova.InAppBrowser.open(
        maximoplus.net.getDownloadURL(this.doclikns, "doclinks", {}),
        "_blank"
      );
      return;
    }
    window.open(
      maximoplus.net.getDownloadURL(this.doclinks, "doclinks", {}),
      "_blank"
    );
  }

  render({ props, state }) {
    if (!state.doclinks) return null;
    return (
      <max-list
        norows="10"
        list-template="doclinks"
        maxcontainer={state.doclinks}
        columns={[
          "document",
          "doctype",
          "description",
          "changeby",
          "changedate"
        ]}
        initdata="true"
        selectableF={ev => this.openDocument()}
      />
    );
  }
}

import { h } from "preact";
import { MPlusComponent } from "./MplusComponent.js";
import { getDeferredContainer, kont } from "../utils.js";
import { props } from "skatejs";

function roundIt(val) {
  return val.toFixed(2);
}

export class DoclinksUpload extends MPlusComponent {
  static get props() {
    return {
      container: props.string,
      maxcontainer: props.object,
      uploadmethod: props.string //if the upload method is not default
    };
  }

  putContainer(container) {
    this.mp = container;
    this.appDocTypeCont = new maximoplus.basecontrols.MboContainer(
      "appdoctype"
    );
    this.appDocTypeCont.setQbe("app", this.mp.getApp());
    let compAdapt = new maximoplus.basecontrols.ComponentAdapter(
      this.appDocTypeCont,
      ["doctype"]
    );
    compAdapt.noRows = 100;
    let that = this;
    compAdapt.onFetchedRow = function(row) {
      let dt = row.data["DOCTYPE"];
      if (dt) {
        if (that.state.doctypes.indexOf(dt) == -1) {
          that.state.doctypes.push(dt);
          that.state = Object.assign({}, this.state);
        }
      }
    };
    compAdapt.initData();
  }
  connected() {
    if (this.mp) {
      return;
    }

    if (this.props.container) {
      getDeferredContainer(this.props.container).then(container => {
        this.putContainer(container);
      });
    }
  }

  getDocumentTypePicker() {
    let options = this.state.doctypes.map(t => <option value={t}>{t}</option>);
    return (
      <div>
        <div>Folder</div>
        <select
          onChange={ev => {
            this.state.doctype = ev.target.value;
            this.state = Object.assign({}, this.state);
          }}
        >
          {options}
        </select>
      </div>
    );
  }

  updated(previousProps) {
    let ret = super.updated(previousProps);
    if (!this.mp && Object.keys(this.props.maxcontainer).length > 0) {
      this.putContainer(this.props.maxcontainer);
      //through hyperscript, this is not set on connected
    }
    return ret;
  }

  render({ props, state }) {
    let dispFiles = state.files.map((f, i) => this.displayFileData(f, i));
    let picker = this.getDocumentTypePicker();
    return (
      <div>
        {picker}
        <input
          ref={i => (this.input = i)}
          type="file"
          style="display:none"
          multiple
          onChange={ev => this.attachFiles(ev.target.files)}
          id="filehidden"
        />
        {dispFiles}
        <button onClick={ev => this.pickFiles()}>Attach</button>
        <button onClick={ev => this.uploadFiles(state.doctype)}>Upload</button>
      </div>
    );
  }

  getAttachButton() {
    //in the mobile webView/browser multiple files selection doesn't work. We have to click the attach button many times to get the result we need. Before the upload, files can be deleted
    //input will be hidden here, so the attach button calls the click on the hidden button and initiates the attaching
  }

  pickFiles() {
    this.input.click();
  }

  attachFiles(files) {
    for (let j = 0; j < files.length; j++) {
      this.state.files.push(files.item(j));
    }
    this.state = Object.assign({}, this.state);
  }

  displayFileData(file, index) {
    let fileSize = this.getFileSize(file);
    return (
      <div>
        {file.name} {fileSize}{" "}
        <span onClick={ev => this.removeFileFromUpload(index)}>&#x2716;</span>
      </div>
    );
  }

  getFileSize(file) {
    var fileSize = file.size;
    if (fileSize < 1024) {
      return fileSize + " B";
    }
    fileSize = fileSize / 1024;
    if (fileSize < 1024) {
      return roundIt(fileSize) + " KB";
    }
    return roundIt(fileSize / 1024) + " MB";
  }

  removeFileFromUpload(index) {
    this.state.files.splice(index, 1);
    this.state = Object.assign({}, this.state);
  }

  uploadFile(file, doctype, docname) {
    let cont = this.props.container
      ? kont[this.props.container]
      : this.props.maxcontainer;
    let uploadMethod = "doclinks";
    if (this.props.uploadmethod) {
      uploadMethod = this.props.uploadmethod;
    }
    let fd = new FormData();
    fd.append("docname", docname);
    fd.append("doctype", doctype);
    fd.append("file", file);
    return new Promise((resolve, reject) => {
      maximoplus.net.upload(
        cont,
        uploadMethod,
        null,
        fd,
        function(ok) {
          resolve(ok);
        },
        function(err) {
          reject(err);
        },
        function(loaded, total) {
          file.percloaded = Math.round(loaded / total);
        }
      );
    });
  }

  uploadFiles(doctype) {
    this.uploadFilesInternal(this.files.slice(), doctype);
  }

  async uploadFilesInternal(files, doctype) {
    this.uploaded = false;

    for (let f of files) {
      try {
        await this.uploadFile(f, doctype, f.name);
      } catch (err) {
        console.log(err);
        this.uploaderror = err;
      }
    }
    this.files = [];
    this.uploaded = true;
  }
}

import { h } from "preact";
import { BaseDialog } from "./BaseDialog.js";
import { filterTemplates } from "./filterTemplates.js";
import { style, getRootEl } from "../utils.js";
import { props } from "skatejs";

export class FilterDialog extends BaseDialog {
  static get props() {
    return {
      dialog: props.object
    };
  }

  getFilter(filtername) {
    return filterTemplates[filtername];
  }

  render({ props }) {
    const filter = this.getFilter(props.dialog.filtername);
    const kont = props.dialog.maxcontainer;
    return this.drawFilter(filter(kont, this));
  }

  drawFilter(filter) {
    return (
      <div>
        {style(this, this.dialogStyle())}
        <div>
          <div class="fadeMe" />
          <div class="popup">{filter}</div>
        </div>
      </div>
    );
  }

  closeDialog() {
    let rootEl = getRootEl();
    const newRootElDialogs = rootEl.dialogs.slice();
    newRootElDialogs.pop();
    rootEl.dialogs = newRootElDialogs;
  }
}

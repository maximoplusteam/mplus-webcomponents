import { h } from "preact";
import { BaseDialog } from "./BaseDialog.js";
import { props } from "skatejs";
import { style } from "../utils.js";
export class GLDialog extends BaseDialog {
  static get props() {
    return {
      orgid: props.string,
      field: props.object
    };
  }

  connected() {
    if (this.mp) return;
    if (Object.keys(this.props.field).length == 0) return;
    this.mp = new maximoplus.re.GLDialog(this.props.field, this.props.orgid);
    this.mp.addWrappedComponent(this);
    this.mp.renderDeferred();
  }

  updated(previousProps) {
    //we call the gldialog always through jsx, never directly, so the properties are going to be passed here
    let ret = super.updated(previousProps);
    if (!this.mp && Object.keys(this.props.field).length > 0) {
      this.mp = new maximoplus.re.GLDialog(this.props.field, this.props.orgid);
      this.mp.addWrappedComponent(this);
      this.mp.renderDeferred();
    }
    return ret;
  }

  getInternalState(property) {
    return this.state && this.state[property];
  }
  /** Sets the internal state
   * @param {function} stateF
   */
  setInternalState(stateF) {
    const prevState = Object.assign({}, this.state);
    const newState = Object.assign({}, stateF(this.state));
    this.state = newState;
    this.pushDialogState(prevState, newState);
  }

  glIndividualSegment({
    listener,
    segmentName,
    segmentValue,
    segmentDelimiter
  }) {
    return (
      <div style="display:inline-block;margin-right:3px;" onClick={listener}>
        <div style="font-size:8px">{segmentName}</div>
        <div style="font-size:15px">{segmentValue + segmentDelimiter}</div>
      </div>
    );
  }

  glSegmentTemplate(segments) {
    return <div>{segments.map(this.glIndividualSegment)}</div>;
  }

  render({ props, state }) {
    let gllist = state.pickerlist ? (
      <max-list
        maxcontainer={state.pickerlist.glcontainer}
        columns={state.pickerlist.pickercols}
        norows="20"
        initdata="true"
        list-template="gllist"
        selectableF={state.pickerlist.pickerf}
      />
    ) : (
      <div />
    );
    return (
      <div>
        {style(this, this.dialogStyle())}
        <div>
          <div class="fadeMe" />
          <div class="popup">
            {this.glSegmentTemplate(this.segments)}
            {gllist}
            <div class="bottomdialog">
              <button
                onClick={ev => {
                  this.chooseF();
                  this.closeDialog();
                }}
              >
                OK
              </button>
              <button onClick={this.closeDialog}>Close</button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

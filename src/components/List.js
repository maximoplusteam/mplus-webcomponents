import { h } from "preact";
import { MPlusComponent } from "./MplusComponent.js";
import { animating } from "../utils.js";
import { props } from "skatejs";

export class List extends MPlusComponent {
  constructor() {
    super();
    animating.map(val => {
      this.setState({ animating: val });
    });
  }

  initData() {
    this.mp.initData();
  }

  putContainer(mboCont) {
    this.mp = new maximoplus.re.Grid(
      mboCont,
      this.props.columns,
      this.props.norows
    );
    this.mp.renderDeferred();
    if (this.props.selectableF && typeof this.props.selectableF == "function") {
      this.mp.setSelectableF(this.props.selectableF);
    }
    this.mp.addWrappedComponent(this);
    if (this.initdata) {
      this.mp.initData();
    }
  }

  static get props() {
    return {
      container: props.string,
      maxcontainer: props.object, //direct container setting from JSX
      columns: props.array,
      norows: props.number,
      initdata: props.boolean,
      listTemplate: props.string,
      filterTemplate: props.string,
      selectableF: props.object //selectableF for dialogs
    };
  }

  updated(previousProps) {
    let ret = super.updated(previousProps);

    if (!this.mp && Object.keys(this.props.maxcontainer).length > 0) {
      this.putContainer(this.props.maxcontainer);
      //through hyperscript, this is not set on connected
    }
    return ret;
  }

  shouldUpdate(prevProps, prevState) {
    if (this.state.animating) {
      return false;
    }
    return true;
  }

  render({ props, state }) {
    let drs = [];
    if (state.maxrows) {
      let template = this.getListTemplate(props.listTemplate);
      if (template) {
        drs = state.maxrows.map(template);
      }
    }

    return this.drawList(drs, this.getFilterButton());
  }

  showFilter() {
    let container = this.props.maxcontainer
      ? this.props.maxcontainer
      : kont[this.props.container];
    this.pushDialog({
      type: "filter",
      maxcontainer: container,
      filtername: this.props.filterTemplate
    });
  }

  //should be overriden
  getFilterButton() {
    if (this.props.filterTemplate) {
      return <button onClick={ev => this.showFilter()}>Filter</button>;
    }
    return <div />;
  }

  //should ne oberriden
  drawList(rows, button) {
    return (
      <div>
        {rows}
        {button}
      </div>
    );
  }
}

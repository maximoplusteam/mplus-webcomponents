import { h } from "preact";
import { BaseDialog } from "./BaseDialog.js";
import { props } from "skatejs";
import { style } from "../utils.js";

export class ListDialog extends BaseDialog {
  static get props() {
    return {
      dialog: props.object //just one dialog at the time, dialog holder will contain more
    };
  }

  render({ props }) {
    return this.drawList(
      <max-list
        norows="10"
        list-template={props.dialog.field.getMetadata().listTemplate}
        filter-template={props.dialog.field.getMetadata().filterTemplate}
        maxcontainer={props.dialog.listContainer}
        initdata="true"
        columns={props.dialog.dialogCols}
        selectableF={props.dialog.defaultAction}
      />
    );
  }

  drawList(list) {
    return (
      <div>
        {style(this, this.dialogStyle())}
        <div>
          <div class="fadeMe" />
          <div class="popup">
            {list}
            <div class="bottomdialog">
              <button onClick={this.props.dialog.closeAction}>
                {this.props.dialog.closeLabel}
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

import { h } from "preact";
import { BaseDialog } from "./BaseDialog.js";
import { style } from "../utils.js";
import { props } from "skatejs";

export class LoginDialog extends BaseDialog {
  static get props() {
    return {
      container: props.string,
      maxcontainer: props.object
    };
  }

  render() {
    return (
      <div>
        {style(this, this.dialogStyle())}
        <div>
          <div class="fadeMe" />
          <div class="popup">
            <div>
              <span>Username:</span>
              <input id="username" />
            </div>
            <div>
              <span>Password:</span>
              <input type="password" id="password" />
            </div>
            <button onClick={ev => this.loginAction()}>Login</button>
          </div>
        </div>
        >
      </div>
    );
  }

  loginAction() {
    let userName = this.shadowRoot.getElementById("username").value;
    let password = this.shadowRoot.getElementById("password").value;
    maximoplus.core.max_login(
      userName,
      password,
      function(ok) {
        document.location.reload();
      },
      function(err) {
        console.log(err);
        maximoplus.core.handleErrorMessage("Invalid Username or Password");
      }
    );
  }
}

import { h } from "preact";
import { Component } from "./Component.js";
import {
  openDialog,
  closeDialog,
  getDeferredContainer,
  resolveContainer
} from "../utils.js";
import { listTemplates } from "./listTemplates.js";

export class MPlusComponent extends Component {
  state = {};
  pushDialogState(previousState, newState) {
    if (newState && newState.maxfields) {
      //	    var rootEl=getRootEl();
      for (let j = 0; j < newState.maxfields.length; j++) {
        const newDialogs = newState.maxfields[j].dialogs;
        if (!newDialogs) {
          continue;
        }
        let prevDialogs =
          previousState.maxfields.length == 0 || !previousState.maxfields[j]
            ? []
            : previousState.maxfields[j].dialogs;
        if (!prevDialogs) {
          prevDialogs = [];
        }

        if (newDialogs.length < prevDialogs.length) {
          this.popDialog();
        }
        if (newDialogs.length > prevDialogs.length) {
          this.pushDialog(newDialogs[0]);
        }
      }
    }
  }

  //the following tho methods should be overriden in the concrete implementations with
  //MPlusComponent.prototype.pushDialog = function (dialog)...

  pushDialog(dialog) {
    //this indirection is necessary, becuase wh can override just the prototype function
    openDialog(dialog);
  }

  connected() {
    if (this.mp) return; //not garantueed to be called only once
    //we cant control the order of the web components in app
    //if this comes before the container:
    if (this.props.container) {
      getDeferredContainer(this.props.container).then(container => {
        this.putContainer(container);
      });
    }
    //if called through hyperscript, it will not be passed here
  }
  /** Method to be called from the core lib
   * @param {string} property
   * @return {object}
   */
  getInternalState(property) {
    return this.state && this.state[property];
  }
  /** Sets the internal state
   * @param {function} stateF
   */
  setInternalState(stateF) {
    const prevState = Object.assign({}, this.state);
    const newState = Object.assign({}, this.state, stateF(this.state));
    this.state = newState;
    this.pushDialogState(prevState, newState);
  }
  popDialog() {
    closeDialog();
  }

  getListTemplate(templateId) {
    //this has to be overriden in the child implementation.
    //List templates will be defined in the separate JSX file , but will be loaded with the main application
    //(maybe again it will just define the list templates variable)
    return listTemplates[templateId];
  }
}

import { h } from "preact";
import { DoclinksUpload } from "./DoclinksUpload.js";
import { style } from "../utils.js";
const isCordovaApp = !!window.cordova;

export class PhotoUpload extends DoclinksUpload {
  constructor() {
    super();
    this.state.files = [];
  }
  takePhoto() {
    if (isCordovaApp && navigator.camera) {
      let options = {
        quality: 50,
        destinationType: Camera.DestinationType.FILE_URI,
        sourceType: Camera.PictureSourceType.CAMERA,
        encodingType: Camera.EncodingType.JPEG,
        mediaType: Camera.MediaType.PICTURE,
        allowEdit: true,
        correctOrientation: true //Corrects Android orientation quirks
      };
      navigator.camera.getPicture(
        pictureUri => {
          this.photos.push(
            <div>
              <img src={pictureUri} />
            </div>
          );
          this.photos = this.photos.slice();
          window.resolveLocalFileSystemURL(
            pictureUri,
            fileEntry =>
              fileEntry.file(f => {
                this.state, files.push(f);
                this.state = Object.assign({}, this.state);
              }),
            error => console.log(error)
          );
        },
        error => {
          console.log(error);
        },
        options
      );
    } else {
      //	    Webcam.snap((data_uri)=>{
      //		this.photos.push(<div><img src={data_uri}></img></div>);
      //		this.photos=this.photos.slice();
      //		let raw_image_data = data_uri.replace(/^data\:image\/\w+\;base64\,/, '');
      //		let arraybuf=decode(raw_image_data);
      //		//	    let blob = new Blob([arraybuf], {type: 'image/jpeg'});
      //		let name=this.getFileName();
      //		let f = new File([arraybuf],name, {type: 'image/jpeg'});//this may not work with the older mobile browsers, check
      //		this.files.push(f);
      //		this.files=this.files.slice();
      //	    });
    }
  }

  getFileName() {
    let d = new Date();
    return "IMG-" + d.valueOf() + ".jpg";
  }

  getWebcamAttachEl() {
    return this.shadowRoot.getElementById("webcam");
  }

  render({ props, state }) {
    let dispFiles = state.files.map((f, i) => this.displayFileData(f, i));
    return (
      <div>
        {style(this, this.imgDisplayStyle())}
        <div>
          <div style="width:300px;height:300px" id="webcam" />
          {dispFiles}
          <button onClick={ev => this.takePhoto()}>Snap</button>
          <button onClick={ev => this.uploadFiles(state.doctype)}>
            Upload
          </button>
        </div>
      </div>
    );
  }

  rendered() {
    if (!navigator.camera) {
      //either webcam or cordova camera plugin is used
      //	    Webcam.attach(this.getWebcamAttachEl() );
    }

    if (!this.doctype) {
      this.doctype = "Photos";
    }
  }

  imgDisplayStyle() {
    return `
img {
    max-width: 100%;
    max-height: 100%;
}

.photo{
height:80px;
width:80px;
}
`;
  }

  displayFileData(file, index) {
    let org = super.displayFileData(file, index);
    let fileSize = this.getFileSize(file);
    let photo = this.photos[index];
    return (
      <div>
        <div class="photo">{photo}</div>
        {org}
      </div>
    );
  }

  removeFileFromUpload(index) {
    super.removeFileFromUpload(index);
    this.state.photos.splice(index, 1);
    this.state = Object.assign({}, this.state);
  }
}

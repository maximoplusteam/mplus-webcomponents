import { h } from "preact";
import { List } from "./List.js";
import { props } from "skatejs";

export class PickerList extends List {
  putContainer(mboCont) {
    super.putContainer(mboCont);
    super.initData();
    this.props.maxpickerfield.addPickerList(this.mp);
  }

  static get props() {
    return {
      ...super.props,
      ...{
        label: props.string,
        pickerkeycol: props.string,
        pickercol: props.string,
        changeListener: props.object,
        maxpickerfield: props.object
      }
    };
  }

  render({ props, state }) {
    let drs = [];
    if (state.maxrows) {
      let that = this;
      drs = state.maxrows.map(function(object, i) {
        let selected = object.picked ? "selected" : "";
        let optionKey = object.data[that.pickerkeycol.toUpperCase()];
        let optionVal = object.data[that.pickercol.toUpperCase()];
        let ret = <option value={optionKey}>{optionVal}</option>;
        if (object.picked) {
          ret = (
            <option selected="true" value={optionKey}>
              {optionVal}
            </option>
          );
        }
        return ret;
      });
    }
    return (
      <div>
        <div>{props.label}</div>
        <select onChange={ev => props.changeListener(ev.target.value)}>
          {drs}
        </select>
      </div>
    );
  }
}

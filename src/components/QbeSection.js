import { h } from "preact";
import { MPlusComponent } from "./MplusComponent.js";
import { animating, getDeferredContainer, resolveContainer } from "../utils.js";
import { props } from "skatejs";

export class QbeSection extends MPlusComponent {
  constructor() {
    super();
    animating.map(val => {
      this.state = Object.assign({}, this.state);
      this.state.animating = val;
    });
  }

  shouldUpdate(prevProps, prevState) {
    //	return prevState.value !== this.state.value;
    if (this.state.animating) {
      return false;
    }
    return true;
  }

  static get props() {
    return {
      container: props.string,
      columns: props.array,
      metadata: props.object,
      qbePrepends: props.array,
      maxcontainer: props.object, //for filters, we don't know the container in advance, the filter will be the function of a container, so the container will be passed to the component
      filterDialog: props.any, //if the qbe section is called with the filterDialog set, display one more field (close or cancel), and or search close the dialog
      afterSearch: props.object //like, open the new tab after the search
    };
  }

  putContainer(mboCont) {
    if (!mboCont || !this.props.columns || this.props.columns.length == 0)
      return;
    this.mp = new maximoplus.re.QbeSection(mboCont, this.props.columns);

    /*
	 Important.
	 The QbeSection in MaximoPlus core library is the only component where column may be the string or the javascript object. The case for javascript object is when we want to search the range in QbeSection. For that we use the standard Maximo functionality - qbePrepend. The columns have to be registered when creating the QbeSection, and the qbePrepend data has to be sent with them, this is why we have that exception. For the case of the components registered with the markup (HTML or JSX, for the web components or React), we already have the metadata defined at the same time as the columns, so we can read this from the metadata itself, and send to the  MaximoPlus constructor.
	 */

    if (this.props.qbePrepends) {
      for (let qp of this.props.qbePrepends) {
        this.mp.addPrependColumns(
          qp.virtualName,
          qp.qbePrepend,
          qp.attributeName,
          qp.title,
          parseInt(qp.position)
        );
      }
    }

    if (this.props.metadata) {
      this.mp.addColumnsMeta(this.props.metadata);
    }

    this.mp.addWrappedComponent(this);
    this.mp.renderDeferred();
    this.mp.initData();
  }

  connected() {
    if (this.mp) return;

    if (this.props.container) {
      getDeferredContainer(this.props.container).then(container => {
        this.putContainer(container);
      });
    }
  }

  clear() {
    this.mp.clearQbe();
  }

  search() {
    this.mp.getContainer().reset(); //i dont' directly access container, becuase it could have been passed as an attribute through HTML, or directly as an object through JSX
    if (this.props.filterDialog) {
      this.propps.filterDialog.closeDialog();
    }
  }

  getSearchButtons() {
    //this may not be necessary, it will render the search buttons for the dialog
    let buttons = [
      { label: "Search", action: ev => this.search() },
      { label: "Clear", action: ev => this.clear() }
    ];
    if (this.props.filterDialog) {
      buttons.push({
        label: "Cancel",
        action: ev => this.props.filterDialog.closeDialog()
      });
    }
    return this.renderSearchButtons(buttons);
  }

  renderSearchButtons(buttons) {
    let rbs = buttons.map(button => (
      <button onClick={button.action}>{button.label}</button>
    ));
    return <div>{rbs}</div>;
  }

  updated(previousProps) {
    let ret = super.updated(previousProps);
    /*
	 We are putting the dialogs as the state on the level of the field. The problem is that in almost all mobile web frameworks, and in React Native as well, it is optimal to have only one level for displaying dialogs, and usually that is the full page. That page is usually on the root level, so we have to update the global state. If we want to display the dialog next to the field(in case of the web app, we can still implement it as a page dialog with the transparent background and the dialog draw next to the field. Here we just need to detect newly open or closed dialogs, and change the global status
	 Since all the dialogs are modal, we can use the stack of dialogs on the global level
	 */
    if (!this.mp && Object.keys(this.props.maxcontainer).length > 0) {
      this.putContainer(this.props.maxcontainer);
      //through hyperscript, this is not set on connected
    }
    this.pushDialogState(previousProps);
    return super.updated(previousProps);
  }

  render() {
    let flds = [];
    const contId = this.props.container;
    let buttons = this.getSearchButtons();

    if (this.state.maxfields) {
      flds = this.state.maxfields.map(function(f) {
        let attrs = {
          label: f.metadata.title,
          value: f.data,
          type: f.metadata.maxType,
          enabled: true,
          listener: f.listeners["change"]
        };
        if (f.metadata.hasLookup) {
          attrs.showLookupF = () => f.maximoField.showLookup();
          attrs.qbe = true; //in qbe mode display only the text field, not the checkbox
          if (f.metadata.offlineReturnColumn && !f.metadata.storeOffline) {
            getDeferredContainer(contId).then(container => {
              maximoplus.basecontrols.addOfflineListReturnColumn(
                container,
                f.column,
                f.metadata.offlineReturnColumn
              );
            });
          }
          if (f.metadata.storeOffline) {
            getDeferredContainer(contId).then(container => {
              maximoplus.basecontrols.listToOffline(
                container,
                f.column,
                f.metadata.listColumns
                  ? f.metadata.listColumns
                  : ["VALUE", "DESCRIPTION"],
                f.metadata.offlineReturnColumn,
                false
              );
            });
          }
        }
        return h("text-field", attrs); //text-field may be used as well for qbe, as it is just a stateless component
      });
    }

    return this.drawFields(flds, buttons);
  }

  drawFields(fields, buttons) {
    //to be implemented in subclass
    return (
      <div>
        {fields}
        {buttons}
      </div>
    );
  }
}

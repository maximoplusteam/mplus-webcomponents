import { h } from "preact";
import { PickerList } from "./PickerList.js";

export class RadioButton extends PickerList {
  render({ props, state }) {
    let drs = [];
    if (state.maxrows) {
      drs = state.maxrows.map(function(object, i) {
        let optionKey = object.data[props.pickerkeycol.toUpperCase()];
        let optionVal = object.data[props.pickercol.toUpperCase()];
        if (object.picked) {
          return (
            <label style="display:block">
              {" "}
              <input
                name={props.label}
                checked="checked"
                value={optionKey}
                onChange={ev => props.changeListener(ev.target.value)}
                type="radio"
              />
              {optionVal}
            </label>
          );
        }
        return (
          <label style="display:block">
            <input
              name={props.label}
              value={optionKey}
              onChange={ev => props.changeListener(ev.target.value)}
              type="radio"
            />
            {optionVal}
          </label>
        );
      });
    }
    return (
      <fieldset>
        <legend>{props.label}</legend>
        {drs}
      </fieldset>
    );
  }
}

import { h } from "preact";
import { Component } from "./Component.js";
import { resolveContainer, getDeferredContainer } from "../utils.js";

export class RelContainer extends Component {
  connected() {
    if (this.mp) return; //not garantueed to be called only once

    getDeferredContainer(this.props.container).then(mboCont => {
      this.mp = new maximoplus.basecontrols.RelContainer(
        mboCont,
        this.props.relationship
      );
      resolveContainer(this.id, this.mp);
    });
  }

  static get props() {
    return {
      container: { attribute: true },
      relationship: { attribute: true },
      id: { attribute: true }
    };
  }
  render() {
    return <div />;
  }

  disconnected() {
    this.mp.dispose();
    delete kont[this.id];
  }
  mboCommand(command) {
    this.mp.mboCommand(command);
  }

  mboSetCommand(command) {
    this.mp.mboSetCommand(command);
  }
}

import { h } from "preact";
import { MPlusComponent } from "./MplusComponent.js";
import { animating, getDeferredContainer, resolveContainer } from "../utils.js";
import { props } from "skatejs";

export class Section extends MPlusComponent {
  constructor() {
    super();
    animating.map(val => {
      this.state = Object.assign({}, this.state);
      this.state.animating = val;
    });
  }

  shouldUpdate(prevProps, prevState) {
    //	return prevState.value !== this.state.value;
    if (this.state.animating) {
      return false;
    }
    return true;
  }

  static get props() {
    return {
      container: props.string,
      maxcontainer: props.object, //directly set container from JSX instead of using attributes in HTML
      columns: props.array,
      metadata: props.object
    };
  }

  putContainer(mboCont) {
    if (!mboCont || !this.props.columns || this.props.columns.length == 0)
      return;
    this.mp = new maximoplus.re.Section(mboCont, this.props.columns);
    if (this.metadata) {
      this.mp.addColumnsMeta(this.props.metadata);
    }
    this.mp.addWrappedComponent(this);
    this.mp.renderDeferred();
  }

  connected() {
    if (this.mp) return; //not garantueed to be called only once
    if (this.props.container) {
      getDeferredContainer(this.props.container).then(container => {
        this.putContainer(container);
      });
    }
  }

  updated(previousProps) {
    let ret = super.updated(previousProps);
    /*
	 We are putting the dialogs as the state on the level of the field. The problem is that in almost all mobile web frameworks, and in React Native as well, it is optimal to have only one level for displaying dialogs, and usually that is the full page. That page is usually on the root level, so we have to update the global state. If we want to display the dialog next to the field(in case of the web app, we can still implement it as a page dialog with the transparent background and the dialog draw next to the field. Here we just need to detect newly open or closed dialogs, and change the global status
	 Since all the dialogs are modal, we can use the stack of dialogs on the global level
	 */

    /*
	 unlike in core library we will enable in-line modification of proeprties. That means that if the container property is changed or the columns are changed, we need to draw the new section. This is required, becuase in general case it can happen through JSX. For example, take a look at the Workflow component - at the core of the dialog is the workflow section. This section can have either reassingment container or the complete wf container, and the fields are diferrent. In the imperative core library we delete the seciton and add it again to the dom. Here, just the JSX properties has been changed, and the section needs to be redrawn
	 */

    if (
      previousProps &&
      Object.keys(this.maxcontainer).length > 0 &&
      previousProps.maxcontainer != this.props.maxcontainer
    ) {
      this.state = Object.assign({}, this.state);
      this.state.maxfields = [];
      delete this.mp;
    }

    if (
      previousProps &&
      this.container &&
      previousProps.container != this.props.container
    ) {
      this.state = Object.assign({}, this.state);
      this.state.maxfields = [];
      delete this.mp;
    }

    if (!this.mp && Object.keys(this.props.maxcontainer).length > 0) {
      this.putContainer(this.props.maxcontainer);
      //through hyperscript, this is not set on connected
    }

    if (!this.mp && this.props.container) {
      //we can pass id even through JSX, like for the Workflow
      if (this.container) {
        getDeferredContainer(this.props.container).then(container => {
          this.putContainer(container);
        });
      }
    }

    if (previousProps && previousProps.metadata != this.props.metadata) {
      if (this.mp) {
        this.mp.addColumnsMeta(this.props.metadata);
      } else {
        getDeferredContainer(this.props.container).then(container => {
          this.putContainer(container);
        });
      }
    }

    this.pushDialogState(previousProps);
    return ret;
  }

  render() {
    let flds = [];
    const contId = this.props.container;
    if (this.state.maxfields) {
      flds = this.state.maxfields.map(function(f) {
        if (f.metadata.picker && f.picker) {
          let lst = f.picker.list;
          if (lst) {
            return h("max-radio-group", {
              label: f.metadata.title,
              maxcontainer: lst.listContainer,
              selectableF: lst.selectableF,
              norows: lst.maxRows,
              pickercol: lst.pickerCol,
              pickerkeycol: lst.pickerKeyCol,
              columns: [lst.pickerKeyCol, lst.pickerCol],
              changeListener: f.listeners["change"],
              maxpickerfield: f.maximoField,
              enabled: !f.readonly,
              required: f.required
            });
          } else {
            return h("div");
          }
        } else {
          let attrs = {
            label: f.metadata.title,
            value: f.data,
            type: f.metadata.maxType,
            listener: f.listeners["change"],
            enabled: f.enabled,
            required: f.required
          };
          if (f.metadata.hasLookup) {
            if (f.metadata.gl) {
              attrs.showLookupF = () => f.maximoField.showGlLookup();
            } else {
              attrs.showLookupF = () => f.maximoField.showLookup();
              if (f.metadata.offlineReturnColumn && !f.metadata.storeOffline) {
                getDeferredContainer(contId).then(container => {
                  maximoplus.basecontrols.addOfflineListReturnColumn(
                    container,
                    f.column,
                    f.metadata.offlineReturnColumn
                  );
                });
              }
              if (f.metadata.storeOffline) {
                getDeferredContainer(contId).then(container => {
                  maximoplus.basecontrols.listToOffline(
                    container,
                    f.column,
                    f.metadata.listColumns
                      ? f.metadata.listColumns
                      : ["VALUE", "DESCRIPTION"],
                    f.metadata.offlineReturnColumn,
                    false
                  );
                });
              }
            }
          }
          return h("text-field", attrs);
          //I can leave this as it is, and in the implementations I can create the variants based on metadata(for example by default it will be the checkbox by default for the YORN types
        }
      });
    }

    return this.drawFields(flds);
  }

  drawFields(fields) {
    //the idea is to make this extensible, this can be the wrapper for the  section
    return <div>{fields}</div>;
  }
}

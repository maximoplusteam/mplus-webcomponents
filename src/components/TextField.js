import { h } from "preact";
import { MPlusComponent } from "./MplusComponent.js";
import { props } from "skatejs";

export class TextField extends MPlusComponent {
  static get props() {
    return {
      label: props.string,
      value: props.string,
      listener: props.object,
      enabled: props.boolean,
      required: props.boolean,
      showLookupF: props.object,
      qbe: props.boolean, //set automatically
      type: props.string
    };
  }

  handleChange(ev) {
    console.log(ev.target.value);
    this.listener(ev.targetValue);
  }

  render({ props }) {
    let lookup =
      props.showLookupF && typeof props.showLookupF == "function" ? (
        <span onClick={props.showLookupF}>&#9167;</span>
      ) : (
        ""
      );
    return (
      <div>
        <div class="label">{this.label}</div>
        <div>
          {lookup}
          <input
            value={props.value}
            onChange={ev => props.listener(ev.target.value)}
          />
        </div>
      </div>
    );
  }
}

import { h } from "preact";
import { BaseDialog } from "./BaseDialog.js";
import { props } from "skatejs";
import { getDeferredContainer } from "../utils.js";

export class WorkflowDialog extends BaseDialog {
  /*
     Workflow compopnent doesn't have to be a dialog, however, it is closing once the workflow has been finished
     */
  static get props() {
    return {
      container: props.string,
      processname: props.string,
      maxcontainer: props.object,
      open: props.boolean,
      title: props.string,
      actions: props.object,
      memo: props.object,
      section: props.object,
      warnings: props.array,
      error: props.object
    };
  }

  putContainer(mboCont) {
    if (this.mp) return;
    this.mp = new maximoplus.re.WorkflowControl(mboCont, this.processname);
    this.mp.addWrappedComponent(this);
    this.mp.routeWf();
  }

  connected() {
    if (this.mp) return;

    if (this.container) {
      getDeferredContainer(this.container).then(container => {
        this.putContainer(container);
      });
    }
  }

  updated(previousProps) {
    let ret = super.updated();

    if (!this.mp && Object.keys(this.maxcontainer).length > 0) {
      this.putContainer(this.maxcontainer);
    }

    if (this.container) {
      getDeferredContainer(this.container).then(container => {
        this.putContainer(container);
      });
    }

    return ret;
  }

  getInternalState(property) {
    return this[property];
  }

  setInternalState(property, state) {
    this[property] = state;
  }
  render({ props, state }) {
    if (!state.section || !state.section.fields || !state.actions) {
      return <div />;
    }
    let fields = Object.keys(state.actions).map(function(key) {
      return {
        key: key,
        actionFunction: state.actions[key].actionFunction,
        label: state.actions[key].label
      };
    });

    let metadata = {
      ACTIONID: {
        picker: "true",
        pickerkeycol: "actionid",
        pickercol: "instruction",
        pickerrows: "10"
      }
    };

    if (this.section.objectName == "REASSIGNWF") {
      metadata = {
        ASSIGNEE: { hasLookup: "true", listTemplate: "personlist" }
      };
    }
    return (
      <div>
        <div>{this.title}</div>
        <max-section
          maxcontainer={this.section.container}
          columns={this.section.fields}
          metadata={metadata}
        />
        {fields}
      </div>
    );
  }
  drawWorfklow(title, section, buttons) {
    const btns = buttons.map(b => {
      return <button onClick={b.actionFunction}>{b.label}</button>;
    });
    return (
      <div>
        <div>{title}</div>
        {section}
        {btns}
      </div>
    );
  }
}

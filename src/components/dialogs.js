import { h } from "preact";

export const dialogs = {
  list: dialog => <list-dialog dialog={dialog} />,
  qbelist: dialog => <list-dialog dialog={dialog} />,
  filter: dialog => <filter-dialog dialog={dialog} />,
  gl: dialog => (
    <gl-dialog
      field={dialog.field}
      orgid={dialog.orgid}
      defaultAction={dialog.defaultAction}
      closeAction={dialog.closeAction}
    />
  ),
  workflow: dialog => (
    <max-workflow
      container={dialog.container}
      processname={dialog.processname}
    />
  ),
  login: dialog => <login-dialog />
};

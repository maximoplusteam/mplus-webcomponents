import { h } from "preact";

//we don't need to subclass the list to display the diferrent list template, just pass the list template to metadata. However, if we want to filter the list, we have to know the filter name. One simple way is to just provide a lookup table with the template name as a key. Another simple way is to have the same names for filter and list templates
export const filterTemplates = {
  valuelist: (cont, dialog) => (
    <qbe-section
      maxcontainer={cont}
      filterDialog={dialog}
      columns={["value", "description"]}
    />
  )
};

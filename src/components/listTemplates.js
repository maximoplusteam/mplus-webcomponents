import { h } from "preact";
import { tickData } from "../utils.js";

export const listTemplates = {
  porow: o => (
    <div
      style="
    /* border-bottom: blue; */
    border-bottom-width: 1px;
    border-bottom-style: solid;
    border-bottom-color: grey;
    padding-bottom: 3px;
"
      onClick={o.rowSelectedAction}
    >
      <div>
        {o.data.PONUM} {o.data.STATUS}
      </div>
      <div>Order Date: {o.data.ORDERDATE}</div>
      <div>{o.data.DESCRIPTION}</div>
    </div>
  ),
  valuelist: o => (
    <div onClick={o.rowSelectedAction}>
      <div>{o.data.VALUE}</div>
      <div>{o.data.DESCRIPTION}</div>
    </div>
  ),
  qbevaluelist: o => (
    <div style="display:flex;flex-direction:row; align-items: center; justify-content: center;">
      {tickData(o.data._SELECTED)}
      <div style="flex:5" onClick={o.rowSelectedAction}>
        <div>{o.data.VALUE}</div>
        <div>{o.data.DESCRIPTION}</div>
      </div>
    </div>
  ),
  gllist: o => (
    <div onClick={o.rowSelectedAction}>
      <div>{o.data.COMPVALUE}</div>
      <div>{o.data.COMPTEXT}</div>
    </div>
  ),
  personlist: o => (
    <div onClick={o.rowSelectedAction}>
      <div>{o.data.PERSONID}</div>
      <div>{o.data.DISPLAYNAME}</div>
    </div>
  ),
  doclinks: o => (
    <div onClick={o.rowSelectedAction}>
      <div>
        {o.data.DOCTYPE} {o.data.DOCUMENT} {o.data.DESCRIPTION}
      </div>
      <div>
        {o.data.CHANGEDATE} {o.data.CHANGEBY}
      </div>
    </div>
  )
};

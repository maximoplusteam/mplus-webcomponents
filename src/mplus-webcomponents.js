import { AppContainer } from "./components/AppContainer.js";
import { RelContainer } from "./components/RelContainer.js";
import { MPlusComponent } from "./components/MplusComponent.js";
import { List } from "./components/List.js";
import { TextField } from "./components/TextField.js";
import { Section } from "./components/Section.js";
import { QbeSection } from "./components/QbeSection.js";
import { BaseDialog } from "./components/BaseDialog.js";
import { ListDialog } from "./components/ListDialog.js";
import { listTemplates } from "./components/listTemplates.js";
import { filterTemplates } from "./components/filterTemplates.js";
import { dialogs } from "./components/dialogs.js";
import { AppRoot } from "./components/AppRoot.js";
import { FilterDialog } from "./components/FilterDialog.js";
import { GLDialog } from "./components/GLDialog.js";
import { DialogHolder } from "./components/DialogHolder.js";
import { PickerList } from "./components/PickerList.js";
import { RadioButton } from "./components/RadioButton.js";
import { WorkflowDialog } from "./components/WorkflowDialog.js";
import { DoclinksUpload } from "./components/DoclinksUpload.js";
import { PhotoUpload } from "./components/PhotoUpload.js";
import { DocDownload } from "./components/DocDownload.js";
import { LoginDialog } from "./components/LoginDialog.js";
import { Component } from "./components/Component.js";
import {
  getRootEl,
  getDeferredContainer,
  resolveContainer,
  animating
} from "./utils.js";

customElements.define("app-container", AppContainer);

customElements.define("rel-container", RelContainer);

export function openWorkflow(container, processname) {
  let dialogs = getRootEl().dialogs.slice();
  dialogs.push({
    type: "workflow",
    processname: processname,
    container: container
  });
  getRootEl().dialogs = dialogs;
}

//setting offline and online should alert the user. In Material Design the alert is the Snackbar component

let alertFunction = function(x) {
  alert(x);
};

export function setAlertFunction(alertF) {
  alertFunction = alertF;
}

export function setOffline(offline) {
  maximoplus.core.setOffline(offline);
  if (offline) {
    alertFunction("Device OFFLINE");
  } else {
    alertFunction("Device ONLINE");
  }
}

//this will be called in the template by default
export function listenForOffline() {
  document.addEventListener("online", function(ok) {
    setOffline(false);
  });
  document.addEventListener("offline", function(ok) {
    setOffline(true);
  });
  if (
    !navigaror.online ||
    (navaigator.network && //Cordova if started offline
      navigator.network.connection.type == Connection.NONE)
  ) {
    //this is the case when the app starts offline. It has to be done after creating the container because it will work automatically just if the offline is enabled for at least one container
    setOffline(true);
  }
}

window["openWorkflow"] = openWorkflow; //temporary , for the testing, use proper exports instead
window["setOffline"]=setOffline;
window["listenForOffline"]=listenForOffline;

maximoplus.core.globalFunctions.global_login_function = function(err) {
  let dialogs = getRootEl().dialogs.slice();
  dialogs.push({ type: "login" });
  getRootEl().dialogs = dialogs;
};

export function defineComponents() {
  //all the custom elements not here, are "final", not meant to be overriden
  customElements.define("login-dialog", LoginDialog);
  customElements.define("app-root", AppRoot);
  customElements.define("list-dialog", ListDialog);
  customElements.define("filter-dialog", FilterDialog);
  customElements.define("gl-dialog", GLDialog);
  customElements.define("dialog-holder", DialogHolder);
  customElements.define("max-list", List);
  customElements.define("text-field", TextField);
  customElements.define("max-combo", PickerList);
  customElements.define("max-radio-group", RadioButton);
  customElements.define("max-section", Section);
  customElements.define("qbe-section", QbeSection);
  customElements.define("max-workflow", WorkflowDialog);
  customElements.define("doc-upload", DoclinksUpload);
  customElements.define("photo-upload", PhotoUpload);
  customElements.define("doc-download", DocDownload);
}

export {
  List,
  TextField,
  Section,
  QbeSection,
  BaseDialog,
  ListDialog,
  listTemplates,
  filterTemplates,
  dialogs,
  AppRoot,
  FilterDialog,
  GLDialog,
  DialogHolder,
  PickerList,
  RadioButton,
  WorkflowDialog,
  DoclinksUpload,
  PhotoUpload,
  DocDownload,
  LoginDialog,
  animating,
  MPlusComponent,
  Component
};

window.defineComponents = defineComponents;

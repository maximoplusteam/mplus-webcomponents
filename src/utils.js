import flyd from "flyd";
import { h } from "preact";

export function getRootEl() {
  //if the user doesn't want AppRoot as a root element, he can customize it
  return document.getElementsByTagName("app-root")[0];
}

export function openDialog(dialog) {
  let rootEl = getRootEl();
  let newDialogs = rootEl.dialogs.slice();
  newDialogs.push(dialog);
  rootEl.dialogs = newDialogs;
}

//this will remove the most current dialog from the dialog stack
export function closeDialog() {
  let rootEl = getRootEl();
  let newDialogs = rootEl.dialogs.slice();
  newDialogs.pop();
  rootEl.dialogs = newDialogs;
}

export const animating = flyd.stream(false);

window.containers = {};
export let kont = window.containers;

export function getDeferredContainer(contId) {
  if (kont[contId]) {
    return kont[contId];
  }
  let _resolve, _reject;
  let prom = new Promise(function(resolve, reject) {
    _resolve = resolve;
    _reject = reject;
  });
  prom.resolve = _resolve;
  prom.reject = _reject;
  kont[contId] = prom;
  return prom;
}

export function resolveContainer(contid, container) {
  if (kont[contid]) {
    kont[contid].resolve(container);
  } else {
    kont[contid] = Promise.resolve(container);
  }
}

export function style(elem, css) {
  return <style>{css}</style>;
}

export const tickData = data =>
  data == "Y" ? <div style="flex:1">&#x2713;</div> : <div />;
